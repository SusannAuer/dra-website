---
title: William Okech
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/WilliamOkech.png
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/WilliamOkech.png
     image_alt: William
---

### Topics:

Data Literacy

### Background:

Research Background: Biomedical Engineering, Biotechnology, and
Microscopy and Image Analysis. Education: PhD in Biomedical Engineering
Work Experience: Postdoctoral Research Associate and Science Editor

### Training style:

I prefer to either deliver seminars or lead interactive workshops
depending on the needs of the clients

### Additional info:

In my free time I enjoy taking long walks, cycling, and reading
non-fiction books and biographies

City: Nairobi <br/> Country: Kenya <br/> ☑ Happy to offer services
online <br/> ➢ Travel restrictions: Distance, Current Location
