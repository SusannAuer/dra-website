---
title: Susann Auer
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/SusannAuer.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/SusannAuer.jpg
     image_alt: Susann
---

### Topics:

Open Science, Reproducibility

### Background:

I have a PhD in biology and work as scientist and lecturer at TU Dresden
since more than 10 years. As a lecturer I design and give lectures in
plant biology and teach and supervise students. In 2019 I became part of
the global community-led education initiative Reproducibility for
Everyone. Rigor and reproducibility are the core of modern science yet
most researchers never receive training in how to work reproducibly. All
of my students receive training from me in good data management
practices and it greatly benefits their performance during experiments
and thesis writing. Since 2020 I give workshops for reproducible
research to a wider audience and have so far reached more than 500
scientists.

### Training style:

interactive workshops, seminars

### Additional info:

chicken wrangler

City: Dresden <br/> Country: Germany <br/> ☑ Happy to offer services
online <br/> ➢ Travel restrictions: travel by train is prefered,
shouldn´t take longer than 4 hours.
