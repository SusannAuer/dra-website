---
title: Julia Pauquet
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/JuliaPauquet.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/JuliaPauquet.jpg
     image_alt: Julia
---

### Topics:

Open Science

### Background:

I have a Bachelor’s degree in Psychology and a Master’s in
Neurocognitive Psychology. At my last position as a researcher I was the
Open Science expert for our lab and helped implementing Open Science
practices, as well as educating my colleagues on Open Science topics. I
have attended many Open Science workshops and conferences.

### Training style:

Interactive workshops or seminars

### Additional info:

I am a bit of a bookworm :)

City: Aachen <br/> Country: Germany <br/> ☑ Happy to offer services
online <br/> ➢ Travel restrictions: I’d like to stay in a radius of \<4
hours by train; will have to be reachable by public transport
