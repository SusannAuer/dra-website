---
title: Peter Schmidt
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/PeterSchmidt.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/PeterSchmidt.jpg
     image_alt: Peter
---

### Topics:

Research Software Engineering, Podcasting

### Background:

My scientific background is in physics (particle physics) where I
obtained a PhD and did a post-doc in the 1990s. As it was very software
development heavy, I moved into the private sector where I worked in
different roles and companies for nearly 20 years. In 2019 I returned to
academia as a Senior Research Software Engineer at the University
College London (UCL) in the UK. Which is where I started the Code for
Thought. Since July 2023 I am working on podcasting full time as a
freelancer

### Training style:

I provide companion podcast episodes for in person or online training
courses and classes

### Additional info:

Born in Germany, I moved to the UK/London in 1997 and have been here
since. In 2018 I started learning French and have spent some time
working from France to improve my language skills. I came to podcasting
quite late (in 2020 - in the first year of the pandemic). And what
started as a hobby and side activity turned into a full time occupation,
which I love doing.

City: London <br/> Country: United Kingdom <br/> ☑ Happy to offer
services online <br/> ➢ Travel restrictions: I prefer to travel by
train. UK travel is usually ok, EU travel depending on location
