---
title: ThomasS Hartmann
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/ThomasSHartmann.png
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/ThomasSHartmann.png
     image_alt: ThomasS
---

### Topics:

Open Science, Data Literacy, Research Software Engineering

### Background:

~20 years in IT-Services (System Architecture, Engineering & Strategy),
~10 years in Conflict Cognition, currently working on
AI-Speech-Gesture-Correlation.

### Training style:

Workshops, Seminars, Walk and Talk

### Additional info:

Natural science, natural philosophy, history, geopolitics, intelligence.

City: Pforzheim <br/> Country: Germany <br/> ☑ Happy to offer services
online <br/> ➢ I am happy to travel (No)
