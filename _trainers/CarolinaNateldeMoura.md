---
title: Carolina Natel de Moura
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/CarolinaNateldeMoura.jpg
sidebar:
   - title: ''
     image: ../assets/images/trainer_images/CarolinaNateldeMoura.jpg
     image_alt: Carolina
---

### Topics:

Open Science, Data Literacy, Research Software Engineering, Machine
Learning, AI for Science

### Background:

I’m a postdoctoral researcher applying machine learning methods to
answer scientific questions related to the impact of climate change on
natural and managed ecosystems. As an Open Science aficionada, I love
teaching fellow researchers how to enhance their open research workflow
by incorporating my research experience into training for better
research practices.

### Training style:

I enjoy crafting interactive workshops that prioritize hands-on
learning, enabling participants to learn through active engagement.

### Additional info:

Since I moved to the German Alps, my favorite activities are hiking,
biking and reading a book in the beautiful lakes in my surroundings.

City: Garmisch-Partenkirchen <br/> Country: Germany <br/> ☑ Happy to
offer services online <br/> ➢ I am happy to travel (At train distance
from Munich)
