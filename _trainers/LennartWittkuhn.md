---
title: Lennart Wittkuhn
excerpt: Trainer in Training
header:
   teaser: ../assets/images/trainer_images/LennartWittkuhn.jpg
sidebar:
   -  title: 'Homepage'
      image: ../assets/images/trainer_images/LennartWittkuhn.jpg
      image_alt: Lennart
      text: "[lennartwittkuhn.com/](https://lennartwittkuhn.com/)"
   -  title: "Contact"
      text: "[lennart.wittkuhn@tutanota.com](mailto:lennart.wittkuhn@tutanota.com)"
   -  title: "Languages"                       
      text: "English, German"     
---

### Topics:

Open Science, Version Control, Research Data Management, Data Literacy, Research Software Engineering, Computational Reproducibility

### Background:

Research Data Scientist with a background in Cognitive Neuroscience & Psychology and 5+ years experience in experimental neuroscience, large-scale data analysis, computation, machine learning, research software engineering, lab management, computational reproducibility and open science

### Training style:

Interactive workshops, seminars, lectures, demonstrations

City: Hamburg <br/> 
Country: Germany <br/>
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel to places I can reach by train <br/>
{: .notice}
