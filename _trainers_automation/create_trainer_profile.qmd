---
format: hugo-md

params:
  trainer_last: "Plomp"
  trn: NA
---

```{r setup, echo=FALSE}
library("knitr")
library("kableExtra")
library("googlesheets4")

knitr::opts_chunk$set(echo = FALSE, results = 'asis', 
                      warning = FALSE, message = FALSE)

## Use of cached tokens is ok for googlesheets
options(gargle_oauth_email = TRUE)
```


```{r}
# trns <- read_sheet("https://docs.google.com/spreadsheets/d/1bQ9q1uRT8YY3nbkBx3q5DCG8kPC6eXhih6X3Xa8h54o/edit?resourcekey#gid=223589169")
# 
# names(trns) <- gsub(pattern = "[^a-zA-Z0-9 ]", replacement = "", names(trns))
# names(trns) <- gsub(pattern = " ", replacement = "_", names(trns))
# 
# trn <- subset(trns, Last_Name == params$trainer_last)
trn <- params$trn
```



### Topics:
```{r}
cat(trn$What_broad_areas_would_your_training_be_under_Check_all_that_apply)
```

### Background: 
```{r}
cat(trn$Please_provide_a_short_description_of_your_background_relevant_to_your_intended_training_offerings_eg_education_work_experience_open_science_experience_and_passions_etc)
```


### Training style: 
```{r}
cat(trn$What_is_your_training_style_eg_interactive_workshops_seminars_flipped_classroom_etc)
```
 

 
```{r}
if(trn$additional == "yes") {
  cat("### Additional info:\n")
  cat(trn$Additional_remarks_about_yourself_eg_fun_fact_hobbies_other_activities)
}
```


```{r}
if(grepl("Online", trn$Would_you_like_to_do_online_andor_in_person_trainings)) {
  trn$online <- "Happy to offer services online"
} else {
  trn$online <- "I offer services in person only"
}

if (trn$Would_you_be_willing_to_travel == "No") {
  trn$travel <- "I am currently unable to travel"
} else if (trn$Would_you_be_willing_to_travel == "Yes") {
  if(trn$travel_restrictions == "yes") {
    trn$travel <- paste0("I am happy to travel (",
                      trn$Are_there_any_restrictions_on_travel_ie_distance_mode_of_travel_etc,
                      ")")
  } else {
    trn$travel <- "I am happy to travel"
  }
} else {
  trn$travel <- paste("Travel restrictions:", trn$Are_there_any_restrictions_on_travel_ie_distance_mode_of_travel_etc)
}
```


City: `r trn$City_of_Residence` <br/> 
Country: `r trn$Country_of_Residence` <br/>
&#x2611; `r trn$online` <br/>
&#x27A2; `r trn$travel` 


