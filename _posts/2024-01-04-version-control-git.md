---
title: "Introduction to version control with Git & GitHub"
categories:
  - Research Software
  - Reproducible Research
tags:
  - training
header:
  teaser: /assets/images/post_images/git.png
---

Make your research reproducible with version control.


![image-center](/assets/images/post_images/git.png){: .align-center}

## Topics

Git, a widely used tool in professional software development but also valuable to researchers, allows tracking changes, aiding collaboration, transparency, and project organization.


## Target audience

Researchers at any level. First experience with the command-line and plain-text files, e.g., Markdown is beneficial but not required.

## Course format

1- or 2-day hands-on workshop with live code-along demonstrations, interactive discussions, online quizzes and exercises.

## Key learning objective

Participants will learn Git for streamlined collaboration, version control, code & data management, and research reproducibility.


## Trainer

[Lennart Wittkuhn](../../../../trainers/LennartWittkuhn)

