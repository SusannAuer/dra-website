---
title: "Open Educational Resources with Quarto"
categories:
  - Open Science
tags:
  - training
header:
  teaser: /assets/images/post_images/quarto.png
---

Create FAIR and open learning ressources with ease.

![image-center](/assets/images/post_images/quarto.png){: .align-center}


## Topics

- Introduction to Quarto
- How to create FAIR OER
- "hy Quarto is so powerful for OER
- Using Quarto to create an OER website

## Target audience

Lecturers, teachers or teaching assistants with 
- Basic Git skills and 
- Basic R or Bash skills 

## Course format

2 half-day workshop with hands-on exercises and a Quarto book + slides as learning materials.


## Key learning objective

At the end of this training, participants will be able to create a Quarto book website for their learning materials (similar to [this course](https://berd-nfdi.github.io/BERD-reproducible-research-course/)).



## Trainers

- [Lennart Wittkuhn](../../../../trainers/LennartWittkuhn)
- [Heidi Seibold](../../../../trainers/HeidiSeibold)

