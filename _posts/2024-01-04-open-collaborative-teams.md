---
title: "Open Collaborative Teams"
categories:
  - Team Science
  - Open Science
tags:
  - training
header:
  teaser: /assets/images/post_images/happy-people.png
---

How to lead an open research team.


![image-center](/assets/images/post_images/happy-people.png){: .align-center}

## Topics
- Forming teams
- Principles of agile methodology
- Scrum basics
- Coaching
- Mentoring
- Leadership styles


## Target audience
Team managers (e.g. Professors, Group Leaders, PostDocs/PhD students running own projects, anyone leading a team)


## Course format
Workshop format with materials provided in advance, Self study with guided discussions and reflection on own environment, role playing (1.5 days in person)


## Key learning objective
At the end of this training, participants will be able to understand the stages of team formation and identify different leadership styles, apply mentoring/coaching skills as a team leader, implement infrastructure for running agile teams

## Trainer

[Joyce Kao](../../../../trainers/JoyceKao)
