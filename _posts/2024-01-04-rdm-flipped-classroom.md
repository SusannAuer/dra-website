---
title: "Research Data Management 101"
categories:
  - Data Literacy
  - Open Science
  - Reproducible Research
tags:
  - training
header:
  teaser: /assets/images/post_images/rdm.png
---

From zero to hero in managing and sharing your data.

![image-center](/assets/images/post_images/rdm.png){: .align-center}

## Topics

- Research data management
- FAIR principles in practice
- Efficient research data management strategy


## Target audience

PhD candidates who work with data. 

## Course format

6-week flipped classroom course with 5 assignments and 3 online meetings. Course book similar to [RDM-101 online book](https://estherplomp.github.io/TNW-RDM-101/).  


## Key learning objective

At the end of this training, participants will be able to manage their research data according to best practice.

## Trainers

- [Esther Plomp](../../../../trainers/EstherPlomp)
- [Heidi Seibold](../../../../trainers/HeidiSeibold)

