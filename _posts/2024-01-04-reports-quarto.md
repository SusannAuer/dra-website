---
title: "Reproducible reports with Quarto"
categories:
  - Reproducible Research
  - Data Literacy
  - Research Software
tags:
  - training
header:
  teaser: /assets/images/post_images/quarto.png
---

Generate articles, presentations or websites by integrating text with code.

![image-center](/assets/images/post_images/quarto.png){: .align-center}

## Topics

Quarto, an open-source scientific and technical publishing system to generate articles, presentations or websites by integrating text with code in e.g., Python or R.

## Target audience

Students or researchers with basic skills in R or Python and experience editing plain-text files, e.g., Markdown.

## Course format

Flexible, e.g., 1-day hands-on workshop with live code-along demonstrations, interactive discussions, online quizzes and exercises.


## Key learning objective

At the end of this training, participants will be able to publish reproducible, production quality articles, presentations, websites, blogs, or books in various formats like HTML or PDF.



## Trainer

[Lennart Wittkuhn](../../../../trainers/LennartWittkuhn)

