---
title: "Exploring Unix: Navigating, Automating, and Mining with the Shell"
categories:
  - Research Software
  - Data Literacy
tags:
  - training
header:
  teaser: /assets/images/post_images/shell.png
---

Get started working with the shell.

![image-center](/assets/images/post_images/shell.png){: .align-center}

## Topics

- Navigating the filesystem with the shell
- Working with files and directories
- Automating the tedious with loops
- Counting and mining with the shell
- Working with free text



## Target audience

Students or researchers who are not familiar with the Unix Shell.

## Course format

1 day workshop, live coding/hands-on session 

## Key learning objective

At the end of this training, participants will be able to confidently navigate files, automate tasks, and analyze text using Unix shell commands.


## Trainer

[Rabea Müller](../../../../trainers/RabeaMueller)

