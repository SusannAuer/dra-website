---
title: "Introduction to Python: A Beginner's Guide"
categories:
  - Research Software
  - Data Literacy
tags:
  - training
header:
  teaser: /assets/images/post_images/python.png
---

An introductory guide to Python for research.


![image-center](/assets/images/post_images/python.png){: .align-center}

## Topics

Introduction into:
- Jupyter Notebook
- Python Basics
- Python Library Pandas 
- Metadata via DOI from DataCite API


## Target audience

Students or researchers who are not familiar with Python

## Course format

1 day workshop, live coding/hands-on session 

## Key learning objective

At the end of this training, participants will understand Python syntax and basic concepts, know possibilities for automating data processing using Pandas and be able to use API requests for metadata analysis


## Trainer

[Rabea Müller](../../../../trainers/RabeaMueller)

