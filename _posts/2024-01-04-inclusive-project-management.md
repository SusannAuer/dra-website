---
title: "Inclusive Project Management"
categories:
  - Team Science
tags:
  - training
header:
  teaser: /assets/images/post_images/happy-people.png
---

How to lead an open research team.


![image-center](/assets/images/post_images/happy-people.png){: .align-center}

## Topics
Basics of Project Management (Project Management Institute) with emphasis on collaborative aspects, Cultural Intelligence

## Target audience
Professors, Group Leaders, or PostDocs/PhD students running their own projects.

## Course format
- Online Kickoff 90 minutes
- 2 weeks for self-study
- 1-day in-person workshop
- implementation
- online wrapup 90 minutes



## Key learning objective
At the end of this training, participants will know the stages of projects, be able to distinguish traditional vs. agile PM, have a basic understanding of management/team styles across different cultures.


## Trainer

[Joyce Kao](../../../../trainers/JoyceKao)
