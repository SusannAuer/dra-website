---
title: "Knowledge management in the lab"
categories:
  - Team Science
tags:
  - training
header:
  teaser: /assets/images/post_images/123.png
---

Research group leaders: this is how you can keep the knowledge while the team changes.

![image-center](/assets/images/post_images/123.png){: .align-center}


## Topics

- Roles and responsibilities in research 
- On/off boarding of team members



## Target audience

Supervisors, group leaders, Principal Investigators.

## Course format

Half-day in person for 5-20 participants, hands-on.


## Key learning objective

At the end of this training, participants will be able to 
- Communicate roles and responsibilities
- Develop an onboarding process to integrate new team members
- Implement knowledge preservation srategies during Offboarding


## Trainer

[Esther Plomp](../../../../trainers/EstherPlomp)

