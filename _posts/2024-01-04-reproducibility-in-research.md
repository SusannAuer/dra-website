---
title: "Reproducibility in research"
categories:
  - Reproducible Research
tags:
  - training
header:
  teaser: /assets/images/post_images/reproducible-research.png
---

Introduction to reproducible research.

![image-center](/assets/images/post_images/reproducible-research.png){: .align-center}

## Topics

Make every aspect of the research process reproducible: data management, file naming, electronic notebooks, writing methodology, data and code sharing, bioinformatics, data visualisation, creating image-based figures


## Target audience

Researchers at all career levels

## Course format

1-day in-person or online workshop including all course materials, quizzes, exercises, time for feedback and discussions


## Key learning objective

At the end of this training, participants will be able to use reproducibility tools and create reproducible workflows for their daily work. 



## Trainer

[Susann Auer](../../../../trainers/SusannAuer)

