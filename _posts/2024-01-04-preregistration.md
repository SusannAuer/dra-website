---
title: "Introduction to Preregistration"
categories:
  - Open Science
tags:
  - training
header:
  teaser: /assets/images/post_images/check2.png
---

Take first steps towards preregistering your research.


![image-center](/assets/images/post_images/check2.png){: .align-center}

## Topics

- Why do we need preregistration
- Showcasing and testing of different templates and services
- Dip your toes into your own first registration!

## Target audience

Master students/early PhDs working on their first couple of projects, med/neuro/psych 


## Course format

In person or online, one-time ~4 hours, some hands-on time, ideally students bring their current/upcoming projects to work on


## Key learning objective

At the end of this training, participants will be able to choose a fitting template and create a preregistration for their projects


## Trainer
[Julia Pauquet](../../../../trainers/JuliaPauquet)

