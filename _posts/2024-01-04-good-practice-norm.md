---
title: "How to make good research practices the norm in your research team?"
categories:
  - Team Science
  - Open Science
  - Reproducible Research
tags:
  - training
header:
  teaser: /assets/images/post_images/check1.png
---

Set up your research group for quality research.


![image-center](/assets/images/post_images/check1.png){: .align-center}

## Topics

Ingredients for 
- reproducible research in research teams, 
- successful teamwork, 
- roles and 
- guidelines.


## Target audience

Research groups (optimal) or just the group leaders (optional)

## Course format

1.5 days, in-person workshop + online debrief. Day 1 (half day): Discussion of key concepts and what is missing for each group. Day 2 (full day): Implementation of first ideas. Debrief: 2 months later (90 minutes). 


## Key learning objective

At the end of this training, participants will be able to set up their research group in a way that good practices are easy to follow (even for new group members).

## Trainer

[Heidi Seibold](../../../../trainers/HeidiSeibold)

