---
title: "Make Your Research Reproducible"
categories:
  - Reproducible Research
  - Research Software
tags:
  - training
header:
  teaser: /assets/images/post_images/flipped.png
---

A flipped-classroom course that helps you make your research project reproducible in 5 weeks.

## Topics

Reproducibility, project organization, documentation, version control, publication of materials, …


## Target audience

Early career researchers with basic skills in R or Python.

## Course format

5-week flipped classroom course with videos, a course booklet, weekly tasks, a course chat, accountability buddies, and weekly mastermind sessions. See here for a similar course.

![image-center](/assets/images/post_images/flipped.png){: .align-center}


## Key learning objective

At the end of this training, participants will be able to make their research project reproducible.

## Trainer

[Heidi Seibold](../../../../trainers/HeidiSeibold)

