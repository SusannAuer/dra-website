---
title: "Data visualization in R"
categories:
  - Research Software
  - Data Literacy
tags:
  - training
header:
  teaser: /assets/images/post_images/vis.png
---

Create your first graphics in R.


![image-center](/assets/images/post_images/vis.png){: .align-center}

## Topics

- Data wrangling with the tidyverse
- Data visualization with base graphics and ggplot2
- Principles of data visualization

## Target audience

Students and researchers with a basic background in R programming.

## Course format

Online, 1 day (or 2 half days) workshop, live coding and exercises/quizzes at the end of each section.



## Key learning objective

At the end of this training, participants will be able to wrangle their data into the appropriate format and generate publication-ready visualizations.



## Trainer

[William Okech](../../../../trainers/WilliamOkech)

