---
title: "Research data management with DataLad"
categories:
  - Data Literacy
  - Reproducible Research
  - Research Software
tags:
  - training
header:
  teaser: /assets/images/post_images/rdm.png
---

Use DataLad to manage and version control your data.


![image-center](/assets/images/post_images/rdm.png){: .align-center}

## Topics

DataLad is a free and open source distributed data management system that keeps track of data, facilitates reproducibility & collaboration, and integrates with widely used infrastructure.


## Target audience

Researchers at any level. First experience with the command-line and plain-text files, e.g., Markdown is beneficial but not required.


## Course format

1-day hands-on workshop with live code-along demonstrations, interactive discussions, online quizzes and exercises. 

## Key learning objective

Implementing core concepts of good research data management: version control of code + data, provenance capture, effective managing of data analysis, publication, collaboration.


## Trainer

[Lennart Wittkuhn](../../../../trainers/LennartWittkuhn)

