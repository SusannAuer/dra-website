---
title: "Boosting Research Productivity with Artificial Intelligence"
categories:
  - Data Literacy
tags:
  - training
header:
  teaser: /assets/images/post_images/ai.png
---

Use AI tools for writing, literature and brainstorming.

![image-center](/assets/images/post_images/ai.png){: .align-center}

## Topics

- Introduction to AI in Research. 
- AI assistants for writing and improving texts (including coding). 
- AI for literature review. 
- Brainstorming ideas.


## Target audience

Researchers at any career level.

## Course format

1-day hands-on workshop.


## Key learning objective

At the end of this training, participants will be able to identify the best AI tools for different research tasks, to apply AI in research to improve their productivity. 


## Trainer

[Carolina Natel de Moura](../../../../trainers/CarolinaNateldeMoura)

