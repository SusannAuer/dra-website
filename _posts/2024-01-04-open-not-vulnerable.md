---
title: "Being open without being vulnerable."
categories:
  - Open Science
tags:
  - training
header:
  teaser: /assets/images/post_images/check2.png
---

Operational security for research.

![image-center](/assets/images/post_images/check2.png){: .align-center}


## Topics

- Operational Security and Open Access
- Risk Assessment & Mitigation



## Target audience

Anyone working with computers not familiar with OPSec.

## Course format

Workshop & Consulting: 
- Theory: What is OPSec, why should I care about it and how does it apply to me?
- Application: Assess state of personal devices & projects; apply measures to protect them.



## Key learning objective

At the end of this training, participants will be able to assess their state of security and take steps to address critical vulnerabilities.



## Trainer

[Thomas S. Hartmann/](../../../../trainers/ThomasSHartmann/)

