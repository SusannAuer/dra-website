---
title: "From chaos to publication: Organising your data and code"
categories:
  - Reproducible Research
  - Research Software
  - Data Literacy
tags:
  - training
header:
  teaser: /assets/images/post_images/pipeline.png
---

Get your research project organised efficiently.

## Topics

- Best practices for research data organization and documentation
- Project template
- Reproducibility package 

![image-center](/assets/images/post_images/pipeline.png){: .align-center}

## Target audience

Early career researchers

## Course format

1-day workshop, blending hands-on activities, peer-feedback sessions and expert guidance

## Key learning objective

At the end of this training, participants will be able to efficiently organize and document their data and code. 


## Trainer

[Carolina Natel de Moura](../../../../trainers/CarolinaNateldeMoura)

