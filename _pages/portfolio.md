---
title: "Portfolio"
layout: categories
entries_layout: grid
permalink: /portfolio/
author_profile: true
---

We offer **bespoke trainings** that fit your needs. 
{: style="text-align: center;"}

Want to know more about the range of trainings we offer?    
Here are some training **teasers**.
{: style="text-align: center;"}

**Interested in booking a training or want to know more?**
[Get in touch!](/contact){: .btn .btn--primary}
{: style="text-align: center;"}


