---
permalink: /organisations/
layout: splash
header:
  overlay_color: "#c34685"
author_profile: false
title: "Improve research quality in your organisation"
---
# QUALITY TRAINING

Do you want to bring quality traing for quality research to your organisation? Work with us!

We offer two possibilities for you:

1. **Regular**: Pay per training.
2. **Digital Research Academy Membership**: yearly membership fee for a full package.

---

# MEMBERSHIPS 
<img src="/assets/images/DRA-membership.png" alt="Digital Research Academy Logo." width="200" style="center"/>

We offer memberships to organisations. You pay a yearly fee and receive a fixed
amount of training from the Digital Research Academy within the next 365 days.

| Membership:                |  Silver         |     Gold        |     Platinum        |
|----------------------------|-----------------|-----------------|---------------------|
| **Price***:                |  8140 €         | 16800 €         | >18000 €            |
| [**Training**](#training): |  2 training days | 3 training days  | Individual agreement |
| [**Trained trainers**](#trained-trainers): | 2 trained trainers | 1 Train-the-Trainer program (7-10 trained trainers) | Individual agreement |

*Prices are paid yearly and may be subject to change.   

Each organisational membership comes with one free individual membership 
(basic membership).  
[View individual membership](/learners#membership){: .btn .btn--primary}

## Training
We discuss with you what your needs are and what training format would be best 
for you. You receive one or more trainings that fit your organisation, delivered
by engaging trainers who receive a fair pay. Our general topics are: 

- Open Science, 
- Data Literacy, and 
- Research Software Engineering.

If full training days do not fit your needs, we will find a solution that is 
equvalent in effort on our side and works for you.

## Trained trainers
We not only offer training on Open Science, Data Literacy and Research Software Engineering, 
but teach some of your organisation's members in delivering training themselves. For details,
see our [Train-the-Trainer program](../train-the-trainer).  




# Book a training

Have questions or want to book a training or a membership?  
[Get in touch!](/contact){: .btn .btn--primary .btn--large}

![](../assets/images/star.png)

