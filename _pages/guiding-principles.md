---
layout: splash
permalink: /principles/
author_profile: false
title: "Vision, Mission and Guiding Principles"

header:
  overlay_color: "#5e616c"
  overlay_image: "/assets/images/shovel.jpg"
excerpt: "Quality Training for Quality Research"
---



## Vision

Our aspiration is that by 2030 digital skills will be part of the academic skill set so that everyone in academia, regardless of their role, background or location, can use those skills to contribute to the collective pursuit of knowledge and global benefit, leading to a brighter, more informed future for all.

## Mission

In order to contribute to this vision, the Digital Research Academy strives to be a reputable training partner, dedicated to empowering academics and researchers worldwide with advancing their digital skills. As a community of trainers we aspire to bridge knowledge gaps, cultivate innovation, and drive positive change through continuous learning and re-adapting. We value and foster a culture of **openness, transparency, integrity, equity, exploration and collective benefit**.


## Guiding Principles
Based on these shared values of our community, the following Guiding Principles work as the compass that influences decisions and actions for the Digital Research Academy:

- Together we aim to be an active, inclusive and open community of trainers and learners based on trust, making it a common practice to listen to and support each other and embed inclusive and open practices in our trainings;  
- We want to inspire and create value in different shapes and forms by continuously improving our teaching and course materials together;
- We try to make it as easy as possible to join and to learn. Within our training courses we provide structure and transparency, we encourage active learning and we give constructive feedback;
- We ensure everyone in the network is paid or rewarded for their efforts, we use clear licences for our materials and a clear pricing structure that includes tailored training.

<img src="/assets/images/DRA_Logo/DRA_LogoColourPos.png" alt="Digital Research Academy Logo." width="150" style="float:right"/>
