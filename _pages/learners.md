---
permalink: /learners/
layout: splash
header:
  overlay_color: "#c34685"
author_profile: false
title: "Let's learn together"
---

# TRAINING + EVENTS

Please check our [event page](https://events.digital-research.academy/) for upcoming training programmes, workshops and events or [get in touch](/contact).

---

# MEMBERSHIP

For individuals, we offer a basic membership. 

| Membership:                |   Basic           | 
|----------------------------|-------------------|
| **Price***:                | 365 €             |  
| **You get**: | Access to all self-learning courses  | 

<img src="/assets/images/DRA-membership.png" alt="Digital Research Academy Logo." width="250" style="float:right"/>
We also offer organisational memberships:
[View organisational memberships](/organisations#memberships){: .btn .btn--primary}

